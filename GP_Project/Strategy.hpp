#pragma once
#include <glm/vec3.hpp>

class Strategy
{
public:
	virtual ~Strategy() = default;
	virtual void ComputeMotion(float delta, glm::vec3& position, glm::vec3& velocity) = 0;
};
