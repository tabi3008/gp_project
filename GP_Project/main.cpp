#define GLEW_STATIC
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Camera.hpp"
#include "Model.hpp"
#include "nameof.hpp"
#include "Shader.hpp"
#include "SkyBox.hpp"
#include "Window.h"
#include <glm/glm.hpp> //core glm functionality
#include <glm/gtc/matrix_inverse.hpp> //glm extension for computing inverse matrices
#include <glm/gtc/matrix_transform.hpp> //glm extension for generating common transformation matrices
#include <glm/gtc/type_ptr.hpp> //glm extension for accessing the internal data structure of glm types

const GLuint SHADOW_WIDTH = 8192, SHADOW_HEIGHT = 8192;

// window
gps::Window myWindow;

// // matrices
glm::mat4 model;
glm::mat4 view;
glm::mat4 lightRotation;
glm::mat4 projection;
glm::mat4 lightSpaceTrMatrix;
glm::mat3 normalMatrix;

// light parameters
glm::vec3 lightDir;
glm::vec3 lightColor;
glm::mat3 lightDirMatrix;
glm::vec3 pointLightColor;
GLuint isFogLoc;
GLuint isFlashlightLoc;
GLuint isFogLocSkybox;

// shader uniform locations
GLint modelLoc;
GLint viewLoc;
GLint projectionLoc;
GLint normalMatrixLoc;
GLint lightDirLoc;
GLint lightColorLoc;
GLint lightDirMatrixLoc;
GLint pointLightColorLoc;

GLuint shadowMapFBO;
GLuint depthMapTexture;


// camera
gps::Camera myCamera(
    glm::vec3(0.0f, 5.0f, 4.0f),
    glm::vec3(0.0f, 0.0f, -10.0f),
    glm::vec3(0.0f, 1.0f, 0.0f));

GLfloat cameraSpeed = 0.1f;
GLboolean pressedKeys[1024];

// models
std::map<std::string, Model*> dynamicModels = {
	// { nameof(car), new Model(DynamicBody) },
    { nameof(teapot), new Model(DynamicBody) }
};

std::map<std::string, Model*> staticModels = {
	{ nameof(plane), new Model(StaticBody) }
};

gps::Model3D lightCube;

GLfloat angle;
GLfloat lightAngle = 2.0f;

// shaders
gps::Shader myBasicShader;
gps::Shader lightShader;

float yaw = -90.0f;
float pitch = 0.0f;
float lastX = 400, lastY = 300;
float xoffset, yoffset;
bool firstMouse = true;

// skybox
std::vector<const GLchar*> faces;
gps::SkyBox mySkyBox;
gps::Shader skyboxShader;
gps::Shader depthMapShader;

GLuint isFog = 0;
GLuint isFlashlight = 0;

GLboolean drawWireframe = false;

WindowDimensions dimensions = myWindow.getWindowDimensions();
float last_recorded_time = 0;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR) {
		std::string error;
		switch (errorCode) {
            case GL_INVALID_ENUM:
                error = "INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error = "INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                error = "INVALID_OPERATION";
                break;
            case GL_STACK_OVERFLOW:
                error = "STACK_OVERFLOW";
                break;
            case GL_STACK_UNDERFLOW:
                error = "STACK_UNDERFLOW";
                break;
            case GL_OUT_OF_MEMORY:
                error = "OUT_OF_MEMORY";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error = "INVALID_FRAMEBUFFER_OPERATION";
                break;
        }
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "Window resized! New width: %d , and height: %d\n", width, height);
	//TODO
}

#pragma region Movement

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

	if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            pressedKeys[key] = true;
        } else if (action == GLFW_RELEASE) {
            pressedKeys[key] = false;
        }
    }
}

void updateViewMatrix(gps::Shader shader, glm::mat4 &view, GLint viewLoc)
{
    view = myCamera.getViewMatrix();

    shader.useShaderProgram();
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = ypos - lastY; // reversed since y-coordinates range from bottom to top
    lastX = xpos;
    lastY = ypos;

    float sensitivity = 0.1f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch -= yoffset;

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    myCamera.rotate(pitch, yaw);
}

void processMovement()
{
	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
		//update view matrix
        updateViewMatrix(myBasicShader, view, viewLoc);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
        //update view matrix
        updateViewMatrix(myBasicShader, view, viewLoc);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
        //update view matrix
        updateViewMatrix(myBasicShader, view, viewLoc);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
        //update view matrix
        updateViewMatrix(myBasicShader, view, viewLoc);
        // compute normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

    if (pressedKeys[GLFW_KEY_Q]) {
        angle -= 1.0f;
        // update model matrix for teapot
        model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
        // update normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
    }

    if (pressedKeys[GLFW_KEY_E]) {
        angle += 1.0f;
        // update model matrix for teapot
        model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
        // update normal matrix for teapot
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
    }

    if (pressedKeys[GLFW_KEY_F]) {
        isFog = 1;
    }

    if (pressedKeys[GLFW_KEY_G]) {
        isFog = 0;
    }

    if (pressedKeys[GLFW_KEY_I]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    if (pressedKeys[GLFW_KEY_O]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    if (pressedKeys[GLFW_KEY_P]) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
    }

    if (pressedKeys[GLFW_KEY_B]) {
        drawWireframe = true;
    }

    if (pressedKeys[GLFW_KEY_N]) {
        drawWireframe = false;
    }

    if (pressedKeys[GLFW_KEY_T]) {
        isFlashlight = 1;
    }

    if (pressedKeys[GLFW_KEY_Y]) {
        isFlashlight = 0;
    }

    if (pressedKeys[GLFW_KEY_J]) {
        lightAngle -= 1.0f;
    }

    if (pressedKeys[GLFW_KEY_L]) {
        lightAngle += 1.0f;
    }
}

void setWindowCallbacks()
{
    glfwSetWindowSizeCallback(myWindow.getWindow(), windowResizeCallback);
    glfwSetCursorPosCallback(myWindow.getWindow(), mouseCallback);
    glfwSetKeyCallback(myWindow.getWindow(), keyboardCallback);
    glfwSetInputMode(myWindow.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

#pragma endregion

#pragma region Inits

void initOpenGLWindow()
{
    myWindow.Create(1024, 768, "OpenGL Project Core");
}

void initSkyBox() {
    faces.push_back("skybox/right.tga");
    faces.push_back("skybox/left.tga");
    faces.push_back("skybox/top.tga");
    faces.push_back("skybox/bottom.tga");
    faces.push_back("skybox/back.tga");
    faces.push_back("skybox/front.tga");
    mySkyBox.Load(faces);
}

void initOpenGLState()
{
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
    glEnable(GL_FRAMEBUFFER_SRGB);
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initModels()
{
    // dynamicModels.at(nameof(car))->LoadModel("models/f1_car/Formula_1_mesh.obj");
    // dynamicModels.at(nameof(car))->SetPosition(glm::vec3(0.0f, 10.0f, 0.0f));

    dynamicModels.at(nameof(teapot))->LoadModel("models/teapot/teapot20segUT.obj");
    staticModels.at(nameof(plane))->LoadModel("models/plane/plane.obj");
    lightCube.LoadModel("models/cube/cube.obj");

    // dynamicModels.at(nameof(car))->boundingBoxObject_.LoadModel("models/wireframe_cube/wireframe_cube.obj");
    dynamicModels.at(nameof(teapot))->boundingBoxObject_.LoadModel("models/wireframe_cube/wireframe_cube.obj");
    staticModels.at(nameof(plane))->boundingBoxObject_.LoadModel("models/wireframe_cube/wireframe_cube.obj");
    

    dynamicModels.at(nameof(teapot))->SetPosition(glm::vec3(0.0f, 15.0f, 0.0f));
    staticModels.at(nameof(plane))->SetPosition(glm::vec3(0.0f, -2.0f, 0.0f));

    myCamera.boundingBoxes.insert(std::pair<std::string, Model*>( nameof(teapot), dynamicModels.at(nameof(teapot))));
    //myCamera.boundingBoxes.insert({ nameof(plane), dynamicModels.at(nameof(car))->boundingBoxObject_ });
    myCamera.boundingBoxes.insert(std::pair<std::string, Model*>( nameof(plane), staticModels.at(nameof(plane))));
}

void initShaders()
{
    myBasicShader.loadShader(
        "shaders/basic.vert",
        "shaders/basic.frag");
    // dynamicModels.at(nameof(car))->SetShader(myBasicShader);
    dynamicModels.at(nameof(teapot))->SetShader(myBasicShader);
    staticModels.at(nameof(plane))->SetShader(myBasicShader);

    myBasicShader.useShaderProgram();

    lightShader.loadShader(
        "shaders/lightShader.vert",
        "shaders/lightShader.frag");

    skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
    skyboxShader.useShaderProgram();

    depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");
}


#pragma endregion

void initFBOs()
{
    //generate FBO ID
    glGenFramebuffers(1, &shadowMapFBO);

    //create depth texture for FBO
    glGenTextures(1, &depthMapTexture);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //attach texture to FBO
    glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void initUniforms()
{
    myBasicShader.useShaderProgram();

    modelLoc = glGetUniformLocation(myBasicShader.shaderProgram, "model");

    viewLoc = glGetUniformLocation(myBasicShader.shaderProgram, "view");

    normalMatrixLoc = glGetUniformLocation(myBasicShader.shaderProgram, "normalMatrix");

    lightDirMatrixLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightDirMatrix");

    isFogLoc = glGetUniformLocation(myBasicShader.shaderProgram, "isFog");
    glUniform1i(isFogLoc, isFog);

    isFlashlightLoc = glGetUniformLocation(myBasicShader.shaderProgram, "isFlashlight");
    glUniform1i(isFlashlightLoc, isFlashlight);

    dimensions = myWindow.getWindowDimensions();
    projection = glm::perspective(glm::radians(45.0f), (float)dimensions.width / (float)dimensions.height, 0.1f, 1000.0f);
    projectionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "projection");
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

    lightDir = glm::vec3(0.0f, 1.0f, 1.0f);
    lightDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightDir");
    glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));


    lightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightColor");
    glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

    pointLightColor = glm::vec3(1.0f, 0.0f, 0.0f);
    pointLightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "pointLightColor");
    glUniform3fv(pointLightColorLoc, 1, glm::value_ptr(pointLightColor));

    glm::vec3 pointLightPosition1 = glm::vec3(-5, 6, 5);
    glUniform3fv(glGetUniformLocation(myBasicShader.shaderProgram, "pointLightPosition1"), 1, glm::value_ptr(pointLightPosition1));

    lightShader.useShaderProgram();
    glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    skyboxShader.useShaderProgram();
    view = myCamera.getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

    projection = glm::perspective(glm::radians(45.0f), (float)dimensions.width / (float)dimensions.height, 0.1f, 1000.0f);
    glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
}

//void initBoundingBoxes() {
//    myCamera.boundingBoxes[0] = createBoundingBox(bridge);
//    myCamera.boundingBoxes[1] = createBoundingBox(stoneWall);
//    myCamera.boundingBoxes[2] = createBoundingBox(house);
//    myCamera.boundingBoxes[3] = createBoundingBox(well);
//    myCamera.boundingBoxes[4] = createBoundingBox(rock);
//    myCamera.boundingBoxes[5] = createBoundingBox(tower);
//    myCamera.boundingBoxes[6] = createBoundingBox(wagon);
//    myCamera.boundingBoxes[7] = createBoundingBox(dog);
//    myCamera.boundingBoxes[8] = createBoundingBox(dragon);
//    myCamera.boundingBoxes[9] = createBoundingBox(road);
//    myCamera.boundingBoxes[10] = createBoundingBox(ground);
//
//    /*int objectCount = 11;
//    for (int tree = 1; tree < 8; tree++, objectCount++) {
//        myCamera.boundingBoxes[objectCount] = createBoundingBox(trees[tree]);
//    }*/
//}

#pragma region Rendering

bool checkCollision(Model &firstModel, Model &secondModel)
{
    const bool collisionX = firstModel.GetPosition().x + firstModel.GetSize().x >= secondModel.GetPosition().x &&
        secondModel.GetPosition().x + secondModel.GetSize().x >= firstModel.GetPosition().x;

    const bool collisionY = firstModel.GetPosition().y + firstModel.GetSize().y >= secondModel.GetPosition().y &&
        secondModel.GetPosition().y + secondModel.GetSize().y >= firstModel.GetPosition().y;

    const bool collisionZ = firstModel.GetPosition().z + firstModel.GetSize().z >= secondModel.GetPosition().z &&
        secondModel.GetPosition().z + secondModel.GetSize().z >= firstModel.GetPosition().z;

    return collisionX && collisionY && collisionZ;
}

void handleCollisions()
{
    std::map<std::string, Model*> models;
    models.insert(staticModels.begin(), staticModels.end());
    models.insert(dynamicModels.begin(), dynamicModels.end());

	for (auto dynamicModel : dynamicModels)
	{
        for(auto model : models)
        {
            if (model.first == dynamicModel.first)
                continue;

            if (checkCollision(*dynamicModel.second, *model.second))
            {
                dynamicModel.second->SetVelocity(glm::vec3(0, 0, 0));
            }
        }
	}
}


void drawObjectsInDepthBuffer()
{
    for (auto model : staticModels)
    {
        model.second->SetShader(depthMapShader);
        model.second->CreateModelMatrix(angle);
        model.second->Draw();
    }

    for (auto model : dynamicModels)
    {
        model.second->SetShader(depthMapShader);
        model.second->CreateModelMatrix(angle);
        model.second->Draw();
    }
}

void drawObjects(float timeDelta, gps::Shader shader)
{
    for(auto model : staticModels)
    {
        model.second->SetShader(shader);
        model.second->CreateModelMatrix(angle);
        model.second->Draw(drawWireframe);
    }

    for (auto model : dynamicModels)
    {
        model.second->SetShader(shader);
        model.second->CreateModelMatrix(angle);
        model.second->Draw(drawWireframe);
    }
}

void moveObjects(float timeDelta, gps::Shader shader) {
    for (auto model : staticModels)
    {
        model.second->SetShader(shader);
        model.second->CreateModelMatrix(angle);
        model.second->ComputeMotion(timeDelta);
    }

    for (auto model : dynamicModels)
    {
        model.second->SetShader(shader);
        model.second->CreateModelMatrix(angle);
        model.second->ComputeMotion(timeDelta);
    }
}

glm::mat4 computeLightSpaceTrMatrix()
{
    const GLfloat near_plane = -40.0f, far_plane = 50.0f;
    glm::mat4 lightProjection = glm::ortho(-50.0f, 50.0f, -50.0f, 50.0f, near_plane, far_plane);

    glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
    glm::mat4 lightView = glm::lookAt(myCamera.getCameraTarget() + 1.0f * lightDirTr, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));

    return lightProjection * lightView;
}

void renderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    handleCollisions();

    float time = glfwGetTime();
    float delta = time - last_recorded_time;
    last_recorded_time = time;

    lightSpaceTrMatrix = computeLightSpaceTrMatrix();

    myBasicShader.useShaderProgram();

    glUniform1i(isFogLoc, isFog);


    /****************** render the scene to the depth buffer (first pass) ******************/
    depthMapShader.useShaderProgram();

    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
    glClear(GL_DEPTH_BUFFER_BIT);

    glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"), 1, GL_FALSE,
        glm::value_ptr(lightSpaceTrMatrix));

    model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
    glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

    drawObjectsInDepthBuffer();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    /**************************** render the scene (second pass) ****************************/
    myBasicShader.useShaderProgram();

    glUniform3fv(glGetUniformLocation(myBasicShader.shaderProgram, "spotlightPosition"), 1, glm::value_ptr(myCamera.getPosition()));
    glUniform3fv(glGetUniformLocation(myBasicShader.shaderProgram, "spotlightDirection"), 1, glm::value_ptr(myCamera.getDirection()));

    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightSpaceTrMatrix"), 1, GL_FALSE,
        glm::value_ptr(lightSpaceTrMatrix));

    updateViewMatrix(myBasicShader, view, viewLoc);

    lightDirMatrix = glm::mat3(glm::inverseTranspose(view));
    glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));

    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, dimensions.width , dimensions.height);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, depthMapTexture);
    glUniform1i(glGetUniformLocation(myBasicShader.shaderProgram, "shadowMap"), 3);

    model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
    glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

    normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));

    drawObjects(delta, myBasicShader);
    moveObjects(delta, myBasicShader);

    /************************** draw a white cube around the light **************************/
    lightShader.useShaderProgram();

    glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

    model = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::translate(model, 1.0f * lightDir);
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

    lightCube.Draw(lightShader, false);

    /**************************************** skybox *****************************************/
    skyboxShader.useShaderProgram();
    glUniform1i(glGetUniformLocation(skyboxShader.shaderProgram, "isFog"), isFog);
    mySkyBox.Draw(skyboxShader, view, projection);


}

#pragma endregion

void cleanup()
{
    myWindow.Delete();
    //cleanup code for your own data
}

int main(int argc, const char * argv[])
{
    try {
        initOpenGLWindow();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    initOpenGLState();
    initFBOs();
	initModels();
    initSkyBox();
	initShaders();
	initUniforms();
    setWindowCallbacks();

	glCheckError();
	// application loop
	while (!glfwWindowShouldClose(myWindow.getWindow())) {
        processMovement();
	    renderScene();


		glfwPollEvents();
		glfwSwapBuffers(myWindow.getWindow());

		glCheckError();
	}

	cleanup();

    return EXIT_SUCCESS;
}