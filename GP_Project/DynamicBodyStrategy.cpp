#include "DynamicBodyStrategy.hpp"

#include "gravitational_acceleration.hpp"
#include <glm/vec3.hpp>

DynamicBodyStrategy::DynamicBodyStrategy() = default;
// TODO: handle destructor
DynamicBodyStrategy::~DynamicBodyStrategy() = default;

void DynamicBodyStrategy::ComputeMotion(const float delta, glm::vec3& position, glm::vec3& velocity)
{
	position += 0.1f *velocity * delta;
	velocity -= GRAVITATIONAL_ACCELERATION * delta;
}
